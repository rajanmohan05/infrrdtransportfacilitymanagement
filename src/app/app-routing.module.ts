import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomepageComponent } from './homepage/homepage.component';
import { BookNewRideComponent } from './book-new-ride/book-new-ride.component';
import { MyRidesComponent } from './my-rides/my-rides.component';
import { AuthService } from "./shared/auth.service";


const routes: Routes = [
  {path:"", component: LoginComponent},
  {path:"Home", component: HomepageComponent, canActivate: [AuthService]},
  {path:"BookNewRide", component: BookNewRideComponent, canActivate: [AuthService]},
  {path:"MyRides", component: MyRidesComponent, canActivate: [AuthService]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
