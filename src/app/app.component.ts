import { Component } from '@angular/core';
import { Router, NavigationStart } from "@angular/router";

import { CommonService } from "./shared/common.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  loggedIn = this.commonService.retrieveLoginData
  title = 'InfrrdTransportFacilityManagement';

  constructor(private router: Router, private commonService: CommonService){
    router.events.subscribe((event) => {
      if(event instanceof NavigationStart) {
        this.loggedIn = this.commonService.retrieveLoginData;
        console.log(event instanceof NavigationStart) 
      }
    });
  }

  comeHome(){
    this.router.navigateByUrl("Home");
  }

  logout(){
    this.commonService.storeLoginData = "";
    this.loggedIn = "";
    "You are logged out"
    this.router.navigateByUrl("")
  }

  
}
