import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { AgGridModule } from 'ag-grid-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MyRidesComponent } from './my-rides/my-rides.component';
import { LoginComponent } from './login/login.component';
import { BookNewRideComponent } from './book-new-ride/book-new-ride.component';
import { ButtonRendererComponent } from './shared/button-renderer/button-renderer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    MyRidesComponent,
    LoginComponent,
    BookNewRideComponent,
    ButtonRendererComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([])
  ],
  entryComponents: [ButtonRendererComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
