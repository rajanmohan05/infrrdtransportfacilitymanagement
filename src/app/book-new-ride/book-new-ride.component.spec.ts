import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookNewRideComponent } from './book-new-ride.component';

describe('BookNewRideComponent', () => {
  let component: BookNewRideComponent;
  let fixture: ComponentFixture<BookNewRideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookNewRideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookNewRideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
