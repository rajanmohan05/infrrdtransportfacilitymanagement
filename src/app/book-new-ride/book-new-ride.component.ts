import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import * as moment from 'moment';
import { Router } from "@angular/router";

import { CommonService } from "../shared/common.service";

@Component({
  selector: 'app-book-new-ride',
  templateUrl: './book-new-ride.component.html',
  styleUrls: ['./book-new-ride.component.css']
})
export class BookNewRideComponent implements OnInit {

  today = new Date();
  bookingForm: FormGroup;
  loginData;
  pickupTimeArr;
  locationArr = [];
  locationArrD = [];
  vehicleTypeArr;
  vehicleArr;
  currentHr = moment(this.today).format("HH");
  currentDate = moment(this.today).format("DD-MM-YYYY");
  defaultPickupTime;
  defaultPickupLocation;
  defaultDropLocation;
  defaultVehicleType;
  defaultVehicle;

  constructor(private fb: FormBuilder, private commonService: CommonService, private router:Router) { }

  ngOnInit() {
    this.loginData = this.commonService.retrieveLoginData;
    
    this.loadBookingForm();
    this.loadPickUpTime();
    this.loadLocation();
    this.loadVehicleType();
  }

  loadBookingForm(){
    this.bookingForm = this.fb.group({
      pickupTime: "",
      pickupLocation: "",
      dropLocation: "",
      vehicleType: "",
      vehicleModel: "",
    })
  }

  loadPickUpTime(){
    var tempArr = []
    for (let i = parseInt(this.currentHr)+2; i <= 24; i++) {
      tempArr.push(i+":00");
    }
    this.pickupTimeArr = tempArr;
    this.defaultPickupTime = parseInt(this.currentHr)+2+":"+"00";
  }

  loadLocation(){
    var tempArr = [];
    var tempArr2 = [];
    this.commonService.retrieveLocationData.forEach(element => {
      tempArr.push(element);
      tempArr2.push(element);
    });
    this.locationArr = tempArr;
    this.locationArrD = tempArr2;
    this.locationArrD.splice(0,1);
    this.defaultPickupLocation = this.locationArr[0]
    this.defaultDropLocation = this.locationArrD[0]
  }

  loadVehicleType(){
    var tempObj = this.commonService.retrieveVehicleData;
    var tempArr=[];
    for (const key in tempObj) {
      if (tempObj.hasOwnProperty(key)) {
        tempArr.push(key);
      }
    }
    this.vehicleTypeArr = tempArr;
    this.defaultVehicleType = "any"
    this.loadVehicles(this.defaultVehicleType)
  }
  
  loadVehicles(vehicleType){
    // alert("vehicles load function");
    var tempArr = [];
    if (vehicleType == "any") {
      var tempObj = this.commonService.retrieveVehicleData;
      for (const key in tempObj) {
        if (tempObj.hasOwnProperty(key)) {
          var tempArr2 = this.vehicleList(tempObj[key]);
          tempArr = tempArr.concat(tempArr2)
        }
      }
      
    } else {
      tempArr = this.vehicleList(this.commonService.retrieveVehicleData[vehicleType]);
    }
    // this.vehicleArr = tempArr;
    this.vehicleArr = tempArr;
    this.defaultVehicle = this.vehicleArr[0]; 
    console.log(this.vehicleArr);
  }

  vehicleList(arr){
    var tempArr = [];
    arr.forEach(element => {
      console.log(element.timeSlot);
      // alert(element.timeSlot);
      var addCarTimeFlag = true;
      var addCarLocationFlag = true;
      if (element.vacantSeats <= 0 ) {
        addCarLocationFlag = false;
      } else{
        if (element.timeSlot) {
          var timeSlot = element.timeSlot.split(":")
          // alert(this.defaultPickupTime);
          var bookedTime = this.defaultPickupTime.split(":")
          // alert(bookedTime[0]+" "+timeSlot[0]+" "+"set")
          if ((parseInt(timeSlot[0]) == parseInt(bookedTime[0])+1 || parseInt(timeSlot[0]) == parseInt(bookedTime[0])-1 || parseInt(timeSlot[0]) == parseInt(bookedTime[0]))) {
          }else{
            addCarTimeFlag = false;
          }
        } 
        if (element.pickupLocation && element.dropLocation) {
          if (element.pickupLocation !== this.defaultPickupLocation || element.dropLocation !== this.defaultDropLocation) {
            addCarLocationFlag = false;
          }          
        }
      }
      if (addCarTimeFlag == true && addCarLocationFlag == true) {
        tempArr.push(element);
      }
    });

    return tempArr
  }

  onSubmit(bookingFormData){
    console.log(bookingFormData);

    var bookingInfo = {
      employeeId: this.loginData.employeeId,
      vehicleType: bookingFormData.vehicleModel.vehicleType,
      vehicleNumber: bookingFormData.vehicleModel.vehicleNumber,
      vacantSeats: bookingFormData.vehicleModel.vacantSeats,
      pickUpTime: bookingFormData.pickupTime,
      pickUpPoint: bookingFormData.pickupLocation,
      destination: bookingFormData.dropLocation,
    }
    this.commonService.retrieveVehicleData[bookingInfo.vehicleType].forEach(element => {
      if (element.vehicleNumber == bookingFormData.vehicleModel.vehicleNumber &&  element.employeesloaded.length==0) {
        this.bookingCompletion(bookingInfo, element.vacantSeats);
        alert("Booked");
      }else if (element.vehicleNumber == bookingFormData.vehicleModel.vehicleNumber && element.employeesloaded.length!==0) {
        var bookingComplettionFlag = false;
        element.employeesloaded.forEach(employee => {
          if (employee == this.loginData.employeeId) {
            alert("You have already booked this ride");
            bookingComplettionFlag = true;
            return;
          }else{
          }
        });
        if (bookingComplettionFlag == false) {
          this.bookingCompletion(bookingInfo, element.vacantSeats);
          alert("Booked");
        }        
      }
    });
  }
  
  bookingCompletion(bookingInfoObj, vacantSeats){
    var tempVehicleData = this.commonService.vehicleData;
    var tempBookingData = this.commonService.retrieveBookingData;
    console.log(tempBookingData);
    
    // alert(tempBookingData);
    tempVehicleData[bookingInfoObj.vehicleType].forEach(element => {
      if (element.vehicleNumber == bookingInfoObj.vehicleNumber) {
        element.vacantSeats = vacantSeats-1;
        element.timeSlot = bookingInfoObj.pickUpTime;
        element.pickupLocation = bookingInfoObj.pickUpPoint;
        element.dropLocation = bookingInfoObj.destination;
        element.status = "booked";
        element.employeesloaded.push(bookingInfoObj.employeeId);
        this.commonService.storeVehicleData = tempVehicleData;
      }
    });
    bookingInfoObj.date = this.currentDate;
    bookingInfoObj.status = "booked";
    console.log(bookingInfoObj);

    if (!tempBookingData) {
      tempBookingData ={};
      tempBookingData[bookingInfoObj.employeeId]=[];
      tempBookingData[bookingInfoObj.employeeId].push(bookingInfoObj);
      
    }else if (!tempBookingData[bookingInfoObj.employeeId]) {
      tempBookingData[bookingInfoObj.employeeId]=[];
      tempBookingData[bookingInfoObj.employeeId].push(bookingInfoObj);
    }else if (tempBookingData[bookingInfoObj.employeeId]) {
      tempBookingData[bookingInfoObj.employeeId].push(bookingInfoObj);
    }
    
    this.commonService.storeBookingData = tempBookingData;
    console.log(this.commonService.retrieveBookingData);
    console.log(this.commonService.retrieveVehicleData);
    this.bookingForm.reset;
    this.router.navigateByUrl("Home");
    
  }

  pickupLocationChange(param, param2){
    console.log(param.value);
    var tempArr = [];
    this.commonService.retrieveLocationData.forEach(element => {
      if (element != param.value) {
        tempArr.push(element);
      }
    });
    this.locationArrD = tempArr;
    if (param.value == param2.value) {
      this.defaultDropLocation = this.locationArrD[0];
    }
  }

  dropLocationChange(param, param2){
    console.log(param.value);
    var tempArr = [];
    this.commonService.retrieveLocationData.forEach(element => {
      if (element != param.value) {
        tempArr.push(element);
      }
    });
    this.locationArr = tempArr;
    if (param.value == param2.value) {
      this.defaultPickupLocation = this.locationArr[0];
    }
  }
}
