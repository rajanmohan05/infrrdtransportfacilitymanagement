import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { CommonService } from "../shared/common.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private commonService: CommonService, private router: Router) { }

  ngOnInit() {
    
  }

  newRide(){
    this.router.navigateByUrl("BookNewRide");
  }

  myRides(){
    this.router.navigateByUrl("MyRides");
  }

}
