import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { JsonDataService } from "../shared/json-data.service";
import { CommonService } from "../shared/common.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  employeeArr;
  submitted: boolean = false;

  constructor( private httpService: JsonDataService, private fb: FormBuilder, private router: Router, private commonService: CommonService) { }

  ngOnInit() {
    this.loadLoginForm()
  }

  loadLoginForm(){
    this.loginForm = this.fb.group({
      employeeId: ["INF001",Validators.required],
      password: ["infrrd@123",Validators.required]
    })
  }
    
  onSubmit(loginData){
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    console.log(loginData);
    
    this.httpService.getLogindata().subscribe(
      data => {
        console.log(data);
        this.employeeArr = data.employeesData;
        //NOT USING FOREACH FOR BREAKING THE LOOP
        for (let i = 0; i < this.employeeArr.length; i++) {
          const element = this.employeeArr[i];
          // alert(element.employeeId + element.password)
          if (element.employeeId == loginData.employeeId && element.password == loginData.password)
          {
            alert("You are logged in");
            delete  element.password;
            this.commonService.storeLoginData = element;
            this.commonService.storeLocationData = data.locationData;
            if (!this.commonService.retrieveVehicleData) {
              this.commonService.storeVehicleData = data.vehicleData;
              console.log(this.commonService.retrieveVehicleData);
            };
            this.router.navigateByUrl('/Home');
            return;
          }
        }
        alert("Check ur login credentials");
      },
      error => {
        alert(error)
      }
    )
    
  }
}
