import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { CommonService } from "../shared/common.service";
import {ButtonRendererComponent  } from "../shared/button-renderer/button-renderer.component";

@Component({
  selector: 'app-my-rides',
  templateUrl: './my-rides.component.html',
  styleUrls: ['./my-rides.component.css']
})
export class MyRidesComponent implements OnInit {
  employeeData: object = this.commonService.retrieveLoginData;
  today = new Date;
  currentHr = moment(this.today).format("HH");
  rowData: any;
  frameworkComponents: any;
  
  columnDefs = [
    {
      headerName: "Date",
      field: "date",
      editable:false,
      maxWidth: 100
    },
    {
      headerName: "Pickup Time",
      field: "pickupTime",
      editable:false,
      maxWidth: 80
    },
    {
      headerName: "Pickup Location",
      field: "pickupLocation",
      editable:false,
      maxWidth: 150
    },
    {
      headerName: "Drop Location",
      field: "dropLocation",
      editable:false,
      maxWidth: 150,
      // cellRenderer:function (params) {
        //   console.log("table",params.data.Date);
        //   var date = moment(params.data.Date);
        //   return date.format('DD-MM-YYYY');
        // }
    },
    {
      headerName: "Vehicle Number",
      field: "vehicleNumber",
      editable:false,
      maxWidth: 150
    },
    {
      headerName: "Action",
      field: "status",
      editable:false,
      width: 150,
      cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        onClick: this.onBtnClick1.bind(this),
        label: status,
        // trial: 
      }
    },
    // {
      //   headerName: "Action",
      //   field: "id",
    //   editable:false,
    //   width: 80,
    //   cellRenderer: 'buttonRenderer',
    //   cellRendererParams: {
      //     onClick: this.onBtnClick1.bind(this),
      //     label: 'Delete',
      //   }
      // },
    ];
    constructor(private commonService: CommonService) {
      this.frameworkComponents = {
        buttonRenderer: ButtonRendererComponent,
      }
    }
    
  ngOnInit() {
    this.employeeData = this.commonService.retrieveLoginData;
    this.loadBookingInfoGrid();
  }
  
  loadBookingInfoGrid(){
    var tempArr = []
    var bookingData = this.commonService.retrieveBookingData;
    if (this.employeeData.employeeId ) {
      
      bookingData[this.employeeData.employeeId].forEach(element => {
        console.log(element);
        var timeSlot = element.pickUpTime.split(":")
        if ( parseInt(timeSlot[0])>=parseInt(this.currentHr)-1) {
          var tempObj = {
            "date": element.date,
            "pickupTime": element.pickUpTime,
            "pickupLocation": element.pickUpPoint,
            "dropLocation": element.destination,
            "vehicleNumber": element.vehicleNumber,
            "status": element.status,
          }
          tempArr.push(tempObj);
        }
      });
    }
    this.rowData = tempArr
  }

  onBtnClick1(e) {
    // this.rowDataClicked1 = e.rowData;
    console.log(e.rowData.vehicleNumber)
    console.log(e.rowData.pickupTime)
    console.log(e.rowData.pickupLocation)
    console.log(e.rowData.dropLocation)
    console.log(e.rowData.status)
    console.log(e.val)
    var bookingData = this.commonService.retrieveBookingData;
    if (e.val == "booked") {
      var tempTime = e.rowData.pickupTime.split(":");
      if (parseInt(tempTime[0])==parseInt(this.currentHr) || parseInt(tempTime[0])==parseInt(this.currentHr)+1 || parseInt(tempTime[0])==parseInt(this.currentHr)-1) {
        bookingData[this.employeeData.employeeId].forEach(element => {
          if (e.rowData.vehicleNumber==element.vehicleNumber && e.rowData.pickupTime==element.pickUpTime && e.rowData.pickupLocation==element.pickUpPoint && e.rowData.dropLocation==element.destination && e.rowData.status==element.status ) {
            element.status = "picked";
          }
        });
      }else{
        var tempVar = confirm("You can pick the ride only within the buffer time of 1 hour from pickup time. For the purpose of testing other functions, this can be overridded by clicking 'OK'");
        if (tempVar == true) {
          bookingData[this.employeeData.employeeId].forEach(element => {
            if (e.rowData.vehicleNumber==element.vehicleNumber && e.rowData.pickupTime==element.pickUpTime && e.rowData.pickupLocation==element.pickUpPoint && e.rowData.dropLocation==element.destination && e.rowData.status==element.status ) {
              element.status = "picked";
            }
          });
        }
      }
    }
    if (e.val == "picked") {
      bookingData[this.employeeData.employeeId].forEach((element,index) => {
        if (e.rowData.vehicleNumber==element.vehicleNumber && e.rowData.pickupTime==element.pickUpTime && e.rowData.pickupLocation==element.pickUpPoint && e.rowData.dropLocation==element.destination && e.rowData.status==element.status ) {
          console.log("finished");
          
          element.status = "finished";
          bookingData[this.employeeData.employeeId].splice(index,1);
        }
      });
    }
    this.commonService.storeBookingData = bookingData;
    this.loadBookingInfoGrid();
    
    
  }
}
