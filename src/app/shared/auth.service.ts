import { Injectable } from '@angular/core';
import { Router, CanActivate } from "@angular/router";

import {CommonService } from "./common.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate  {

  constructor(private router: Router, private commonService: CommonService) { }

  canActivate() {
    console.log("OnlyLoggedInUsers");
    if (this.commonService.userData) {
      return true;
    } else {
      window.alert("You have to login to view this page");
      this.router.navigateByUrl("");
      return false;
    }
  }
}
