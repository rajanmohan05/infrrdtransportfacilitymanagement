import { Component } from '@angular/core';
import { ICellRendererAngularComp } from "ag-grid-angular";
// import { ICellRendererParams, IAfterGuiAttachedParams } from "ag-grid";

@Component({
  selector: 'app-button-renderer',
  templateUrl: './button-renderer.component.html',
  styleUrls: ['./button-renderer.component.scss']
})
export class ButtonRendererComponent implements ICellRendererAngularComp {

  params;
  label: string;

  agInit(params): void {
    console.log(params);
    console.log(params.value);
    
    this.params = params;
    this.label = params.value == "booked" ? "Pick my ride" : "picked" ? "End my ride":null;
    console.log("label ",this.label);
  }

  refresh(params?: any): boolean {
    return true;
  }

  delete($event) {
    if (this.params.onClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data,
        val: this.params.value,
        task: "delete",
        // ...something
      }
      this.params.onClick(params);
    }
  }
  edit($event) {
    if (this.params.onClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data,
        val: this.params.value,
        task: "edit",
        // ...something
      }
      this.params.onClick(params);
    }
  }

  

}
