import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  userData;
  vehicleData;
  locationData;
  bookingData;

  constructor() { }

  //STORE LOGIN INFORMATION 
  set storeLoginData(userData: {}){
    this.userData = userData;
  }
  //RETRIEVE LOGIN INFORMATION
  get retrieveLoginData(){
    return this.userData;
  }

  //STORE LOCATION INFORMATION 
  set storeLocationData(locationData){
    this.locationData = locationData;
  }
  //RETRIEVE LOCATION INFORMATION
  get retrieveLocationData(){
    return this.locationData;
  }

  //STORE VEHICLE INFORMATION 
  set storeVehicleData(data: {}){
    this.vehicleData = data;
  }
  //RETRIEVE VEHICLE INFORMATION
  get retrieveVehicleData(){
    return this.vehicleData;
  }

  //STORE BOOKING INFO INFORMATION 
  set storeBookingData(data: {}){
    this.bookingData = data;
  }
  //RETRIEVE BOOKING INFO INFORMATION
  get retrieveBookingData(){
    return this.bookingData;
  }
}
